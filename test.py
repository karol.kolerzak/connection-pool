import time
from connection_pool import ConnectionPool
import random
import threading
import unittest

def testing_connection_pool(connection_pool):
    conn = connection_pool.getconn()
    if conn is not None:
        print("Simulating work")
        time.sleep(random.randint(1, 10))
        connection_pool.close_connection(conn)


class TestUser(unittest.TestCase):

    def test_connection_pool(self):
        test = False
        try:
            connection_pool = ConnectionPool(10, 100)

            start_time = time.time()
            timer = 0

            while timer < 180:
                current_time = time.time()
                timer = current_time - start_time
                print(timer)
                for _ in range(random.randint(1, 10)):
                    thread = threading.Thread(target=testing_connection_pool, args=(connection_pool,))
                    thread.start()
                time.sleep(1)
        except:
            test = True


        self.assertEqual(test, False)


if __name__ == '__main__':
    unittest.main()
